Query 1: Show the songs that have won at least one Grammy award and obtained a certification displaing the song, the grammy, the certification, the belonging artist and the label that produced such song nodes.  

Graph view
MATCH (c:Certification)<-[o:obtained]-(s:Song)-[w:win]->(g:Grammy), (s)-[]->(a:Artist) RETURN a,c,o,s,w,g

Table view (restrict the nodes and relations view only at significative properties) 
MATCH (c:Certification)<-[o:obtained]-(s:Song)-[w:win]->(g:Grammy), (s)-[]->(a:Artist) RETURN a.name AS `Artist`,c.type AS `Certification`,s.title AS `Title`,w.edition AS `Edition`,g.category AS `Category` ORDER BY w.edition

-------------------------------------------------

Query 2: Eminem's album with track list

MATCH (ar:Artist)<-[:performed]-(s:Song)-[:appears]->(al:Album)
WHERE ar.name = "Eminem"
RETURN al.title AS `Album`, collect(s.title) AS `Tracks`, COUNT(s) 

-------------------------------------------------

Query 3: Find which playlist has the longenst duration in millisecond

MATCH (s:Song)-[i:isInside]->(p:Playlist)
RETURN p.title AS Playlist, SUM(apoc.date.convert(s.duration, "ms", "m")) AS Tot
ORDER BY Tot DESC
LIMIT 1

-------------------------------------------------

Query 4: Return the number of occurrences of the most popular song for each artist in the playlists.

MATCH (pl:Playlist)-[ins:isInside]-(s:Song)--(a:Artist)
RETURN a.name AS `Artist`, s.title AS `Title`, COUNT(ins.position) AS `Occurrences`
ORDER BY `Occurrences` DESC

-------------------------------------------------

Query 5: Return the song awards for Drake together with their grammy category or certification type.

MATCH (a:Artist)--(s:Song)--(aw:Award)
WHERE a.name = "Drake" AND aw.category IS NOT NULL
    RETURN s.title AS `Title`,aw.category AS `Award`
UNION ALL
MATCH (a:Artist)--(s:Song)--(aw:Award)
WHERE a.name = "Drake" AND aw.type IS NOT NULL
    RETURN s.title AS `Title`,aw.type AS `Award`

-------------------------------------------------

Query 6: Is the number of award won by female artists grater than the male one?

MATCH (a:Artist)--()--(aw:Award)
WHERE a.gender = "F"
    RETURN a.gender AS `Gender`, count(DISTINCT aw) AS `Num of awards`
UNION ALL
MATCH (a:Artist)--()--(aw:Award)
WHERE a.gender = "M"
    RETURN a.gender AS `Gender`, count(DISTINCT aw) AS `Num of awards`
	
-------------------------------------------------

Query 7: Return the title of the song in position 0 (the first one) for each playlist, and the playlist title.

MATCH (s:Song)-[isn:isInside]-(pl:Playlist)
WHERE isn.position = 0
RETURN pl.id, pl.title AS `Playlist title`, s.title AS `song title`

-------------------------------------------------

Query 8: Return the countries together with the number of awards won by artists 

MATCH (c:Country)--(a:Artist)--()--(aw:Award)
RETURN c.name, COUNT(aw) as `Awards`
ORDER BY `Awards` DESC

-------------------------------------------------

Query 9: Return the genre of the artist that won a grammy. Order by edition and category

MATCH (a:Artist)--(s:Album)-[w:win]-(g:Grammy)
MATCH (a:Artist)-[:hasGenre]-(gen:Genre)
WHEREgen.name <> ""
RETURN a.name AS Name, collect(gen.name) AS Genre, s.title, w.edition, g.category
ORDER BY g.category DESC, w.edition DESC

-------------------------------------------------

USE CASE 1: I'm "Snoop Dogg" and I want to know how many times my songs are present across the various playlists

MATCH (a:Artist)-[]-(s:Song)-[i:isInside]-(p:Playlist)
WHERE a.name = "Snoop Dogg"
RETURN s.title AS title, COUNT(i)

-------------------------------------------------

USE CASE 2: Intescope. 	Another label or an artist wants to know the number of certifications and grammy 
						obtained/won by a label before deciding if it is reasonable to start a collaboration.

						
MATCH p=(l:Label)-[pr:produced]->()-[w:obtained]-()
WHERE l.name =~ "(?i)Interscope.*"
RETURN p, COUNT (w) AS wi

MATCH p=(l:Label)-[pr:produced]->()-[w:win]-()
WHERE l.name =~ "(?i)Interscope.*"
RETURN p, COUNT (w) AS wi

-------------------------------------------------

USE CASE 3: I'm the Interscope records and I want to know how many awards have won the other labels

MATCH (l:Label)-[pr:produced]->()-[wo:win|obtained]-()
WITH l AS label, wo as winob
WHERE NOT label.name =~ "(?i)Interscope.*"
RETURN label.name AS Label, count(winob) AS `Number of received awards`

--------------------------------------------------
						
Query as analytics: Show the 25 artists with the higher number of certification obtained

MATCH p=(a:Artist)-[r:released|performed]-()-[o:obtained]-()
RETURN a.name AS Artist, COUNT(p) AS `Number of obtained certifications`
ORDER BY `Number of obtained certifications` DESC
LIMIT 25

-------------------------------------------------

Analytics 1: Order Genres from the most popular to the least

CALL gds.graph.create.cypher(
  'genre3',
  'MATCH (n) WHERE n:Genre OR n:Artist RETURN id(n) AS id',
  'MATCH (g:Genre)<-[hg:hasGenre]-(a:Artist) RETURN id(g) AS source, id(a) AS target')
YIELD
  graphName AS graph, nodeQuery, nodeCount AS nodes, relationshipQuery, relationshipCount AS rels

CALL gds.degree.stream('genre3')
YIELD nodeId, score
WHERE gds.util.asNode(nodeId).gid IS NOT NULL
RETURN gds.util.asNode(nodeId).name AS name, score AS connections
ORDER BY connections DESC, name DESC

IMPO: The "WHERE" clause is used to distinguish between genre and artist nodes