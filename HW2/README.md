# README #

This is the Graphbusters group's repository for the first homework of the Database 2 course at DEI.

## Team Member

- Alessandro Benetti	- alessandro.benetti.1@studenti.unipd.it
- Andrea Cassetta 		- andrea.cassetta@studenti.unipd.it
- Enrico Vicentini	 	- enrico.vicentini.1@studenti.unipd.it

### Description of our ontology ###

This work is based on the previous group project in which we built an ontology about spotify playlists, modelling the concepts of song, album, playlist, artists
and their mutual relationships.
We expanded the data to include the awards that a song, or an album, may have won and the labels that partecipate in their production. 
This work can be used to explore the realm of playlists in all its aspects.

The purpose of the assignment was to use Neo4j to build our database and interact with it using cypher query language.

### Setup ###

Neo4j Enterprise Edition v. 4.3.9.
MusicDBMS 4.3.9 as Example Project

The code of the workflow, like in the previous homework, is splittend in two part to improve the flexibility of the data management and to be able to work in parallel on differen aspects of the data.
Having done a deep and complex data cleaning/preprocessing in the previous homework it was decided to create our database on top of those data.

**MusicOntology_Cypher:**

Ingestion of the playlists and relatives details (such as the number of tracks and album composing it, the followers and the title).
Ingestion of songs information, like its title and duration, and creation of artists and albums nodes.
Ingestion the awards, Certifications and Grammy, for songs and albums.
Discographic label nodes creation.

**MusicOntology_Cypher2:**

In this notebook we processed the artists' countries and the artists addtional information, like gender, genre, year first album, etc.
Thanks to the functionality provided by Neo4j we have also fixed a little bug which happened in the previous homework.
Using the "MERGE" clause in combination with "NOT exist()" we removed duplicate artist-countries relations, operation that using python would have need many lines of code. 

### Technical Aspects ###

We started from the big dataset provided directly from Spotify. Unfortunaltely it was not possible this time to process the whole dataset. 
The python connector create a direct link to Neo4j and the data are sent and processed as cypher queries. We think that this limit the overall processing speed. 
For this reason we limit the playlists import between the 20 and 22 thousend. Therefore in the database you can find only a little slice of the whole playlist set, and we suggest to stop the python run around this number.
Due to this fact even the queries are performed on this limited number of playlist. 

### Use Cases ###

In the "Queries.cypher" file you can find obviously the queries, some analytics and even some use cases.
In an example we imagine to be the artist, in our case "Snoop Dogg", and we want to know how many times my song are present in the spotify playlists.
In another case we imagine to be a label, "Interscope", and we want to know how many Grammys the songs and albums produced by us have won and obtained for the Certifications, and even how many award have received our rivals.

### Analytics ###

To obtain some analytics we have tried different ways:
- Write some queries containing the COUNT() function to obtain general information about some kind of nodes;
- Use the instruction provided by the neo4j documentation that suggest to use the CALL...YEALD construct, but to run this solution we needed to add the plugin "Graph Data Science Library" to our DBMS, and to project a part of our graph;
- Useing the predefined neo4j plugin "Graph Data Science Playground" that permit to obtain the analytics directly in graphical visualization (the examples can be found in AnalyticsScreen folder);

The first to cases as cited before are present in the file "Queries.cypher".

### Queries ###

For the queries we translate in cypher some of the old queries to show the differences between the the query languages, and we build some other new queries to try the new functionality provided by cypher.
As an example we install and use the APOC plugin for convert the duration on a song starting from milliseconds to minutes. 
