# README #

This is the Graphbusters group's repository for the first homework of the Database 2 course at DEI.

## Team Member

- Alessandro Benetti	- alessandro.benetti.1@studenti.unipd.it
- Andrea Cassetta 		- andrea.cassetta@studenti.unipd.it
- Enrico Vicentini	 	- enrico.vicentini.1@studenti.unipd.it

### Description of our ontology ###

This work aimed to build an ontology about spotify playlists, we modeled the concept of song, album, playlist, artists and their mutual 
relationships.  We expanded the ontology to take into account the awards that a song, or an album, may have won
and the labels that partecipate in their production.  This work can be used to explore the realm of playlists.   

### Setup ###

We started from the big dataset provided directly from Spotify for a machine learning challenge.  
It's a collection composed by one million playlists from which we can retrive information abount the playlist itself (such as the number  
of tracks and album composing it, the followers and the title) and the songs contained.  
From each song we extract information like the title, the duration, the album the artis they belong to, etc.
With all this kind of stuff we built the main classes, properties and data properties from which we start extending our database.
We also used the URIs provided by the spotify dataset to build the IDs of the related individuals (such as Song, Artist, Albums and Playlist).
Then we expanded this dataset with other two:

This one to gather informations about Grammy awards and RIAA certifications.  
*https://www.kaggle.com/danield2255/data-on-songs-from-billboard-19992019.*  

And this one to have info about the country in which the artist was artistically born.    
*https://www.kaggle.com/pieca111/music-artists-popularity*

In this repository there isn't the huge dataset provided by Spotify which can be downloaded following the link below.  
*https://www.aicrowd.com/challenges/spotify-million-playlist-dataset-challenge#dataset*  

The code for the preprocessing and cleaning the data is in the corresponding folder. We decide to build from scratch or to integrate  
some support files to speed up the process of matching between elements of different dataset
adding the Spotify URIs, lowering case some names, and  
this kind of stuff (you can see this files in the BillbordDataset.zip folder).

The folder "dataset" contains some of the source data. The million playlist dataset folder *data* can be downloaded from the previously  
cited link and placed in the "dataset" folder to be able to use it during the ingestion phase.  

The output folder contains all the turtle files which together with the ontology ("Music_Ontology.owl") can be imported directly into graphDB.

We decided to spit the source code into three files to analyze each part of the ontology separately and to me more flexible:  
 - "MusicOntologyPopulation.ipynb" -> Spotify playlists elaboration -> output/playlist-turtle.zip  
 - "MusicOntologyPopulation1.ipynb" -> Award, label and genre elaboration  
 - "MusicOntologyPopulation2.ipynb" -> Country elaboration  

### External Ontologies ###

We decided to import the FOAF ontology, and the eulershap country.

### Queries ###

In the file Queries.pdf you can find 14 examples of qeries with their results, and if you want to try them they are present in the "try.sparql"
from which you can copy and execute them one by one.

### Graph ###

A graphical rapresentation of our ontology can be found opening the "MO_Graph.png" file.


