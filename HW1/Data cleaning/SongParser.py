import sys
import json
import os
import re
from collections import Counter

total_playlists = 0
total_tracks = 0
tracks = set()
artists = set()
albums = set()
titles = set()
total_descriptions = 0
full_track_description = set()

set_artist_desc = set()
set_track_desc = set()
set_album_desc = set()

set_descrizione_totale = set()

dict_full_album_desc = {}

prova = False
numero_file_test = 2


def process_mpd(path):
    count = 0
    filenames = os.listdir(path)
    for filename in sorted(filenames):
        if filename.startswith("mpd.slice.") and filename.endswith(".json"):
            fullpath = os.sep.join((path, filename))
            print(fullpath)
            f = open(fullpath)
            js = f.read()
            f.close()
            mpd_slice = json.loads(js)
            process_info(mpd_slice["info"])
            for playlist in mpd_slice["playlists"]:
                process_playlist(playlist)
            count += 1

            if prova and count > numero_file_test:
                break

    show_summary()


def show_summary():
    print()
    print("number of playlists", total_playlists)
    print("number of tracks", total_tracks)
    print("number of unique tracks", len(tracks))
    print("number of unique albums", len(albums))
    print("number of unique artists", len(artists))
    print("number of unique titles", len(titles))
    print("number of playlists with descriptions", total_descriptions)
    print()


def process_playlist(playlist):
    global total_playlists, total_tracks, total_descriptions

    total_playlists += 1

    if "description" in playlist:
        total_descriptions += 1

    titles.add(playlist["name"])

    for track in playlist["tracks"]:
        total_tracks += 1
        albums.add(track["album_uri"])
        tracks.add(track["track_uri"])
        artists.add(track["artist_uri"])
        artist_uri = track["artist_uri"]

        ##Total extraction
        uri_song = track["track_uri"]

        name_song = track["track_name"]
        nname = normalize_name(name_song)
        name_song = nname

        artist_name_song = track["artist_name"]
        normartist = normalize_name(artist_name_song)
        artist_name_song = normartist

        album_name = track["album_name"]
        normalbum = normalize_name(album_name)
        album_name = normalbum

        album_uri = track["album_uri"]

        duration_song = str(track["duration_ms"])

        artist_description = artist_uri[15:] + "   " + artist_name_song
        song_description = uri_song[14:] + "   " + name_song
        album_desc = album_uri[14:] + "   " + album_name

        descrizione_totale = artist_description + "   " + song_description + "   " + album_desc

        set_track_desc.add(song_description)
        set_artist_desc.add(artist_description)
        set_album_desc.add(album_desc)
        set_descrizione_totale.add(descrizione_totale)

        dict_full_album_desc[album_name + "   " + artist_name_song] = artist_name_song


def normalize_name(name):
    name = name.lower()
    name = re.sub(r"[.,\/#!$%\^\*;:{}=\_`~()@]", " ", name)
    name = re.sub(r"\s+", " ", name).strip()
    return name


def process_info(_):
    pass


if __name__ == "__main__":
    path = sys.argv[1]
    if len(sys.argv) > 2 and sys.argv[2] == "--prova":
        prova = True
    process_mpd(path)

    print("OKOKOK")
    print("Dizionario  ", len(dict_full_album_desc))

    with open("../../Billboard-top100-1999-2019/BillboardFromLast20/DescrizioneCompleta_withID_utf8_giusto.txt", "w",
              encoding='utf-8') as dataset:
        for line in range(len(set_descrizione_totale)):
            a = set_descrizione_totale.pop()
            dataset.write(a + "\n")

